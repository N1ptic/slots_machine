FROM python:alpine3.18
LABEL authors="N1"
WORKDIR /app
COPY main.py /app
ENTRYPOINT ["python", "/app/main.py"]